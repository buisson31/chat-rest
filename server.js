let app = require('express')()
let http = require('http').Server(app)
const messageList = []

let io = require('socket.io')(http, {
   cors: {
      origin: '*',
   }
})
let port = 3000

http.listen(port, () => {
   console.log(`Listening on port *: ${port}`)
})

// serve index.html file
app.get('/', (req, res) => {
   res.sendFile(__dirname + '/index.html')
})

// handle websocket connection
io.on('connection', (socket) => {
   
   socket.on('disconnect', () => {
      console.log("A user disconnected")
   })

   socket.on('userConnected', (username) => {
      console.log(`${username} connected`)
      socket.emit('messagelist', messageList)
   })

   socket.on('newmessage', (data) => {
      console.log('received message', data)
      messageList.push (data)
      socket.broadcast.emit('messagelist', messageList)
      socket.emit('messagelist', messageList)
   })
})
